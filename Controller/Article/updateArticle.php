<?php
$errors = new ArrayObject();
$articleId = getArticleIdFromURI();
$title="updateArticle " . $articleId;

require("../Model/articleRepository.php");

if(!$response = articleExist($articleId)){
    echo "This article does not exist";
    exit();
}

//var_dump($response);die;

if(isFormValid($errors)){

    $author = $_POST['author'];
    $articleTitle = $_POST['title'];
    $body = $_POST['body'];

    $bdd = dbConnect();

    $response = updateArticle($articleId, $author, $articleTitle, $body);

    $response->closeCursor();

//    var_dump($_SERVER);
    header('Location: '.$_SERVER['REDIRECT_URL']);
    die;
}

ob_start();
displayErrors($errors);
//var_dump($response);
$authorId = $response['author_id'];
$body = $response['body'];
$articleTitle = $response['title'];
require("../view/article/updateArticleView.php");

$content=ob_get_clean();

require("../view/templateView.php");




function getArticleIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}

function articleExist($articleId){
    if($articleId == 0){
        return false;
    }
    return getArticle($articleId)->fetch();

}

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }

    $author = $_POST['author'];
    $title = $_POST['title'];
    $body = $_POST['body'];
    if(!fieldsArefilled($author, $body, $title)){
        $errors->append('All the fields must be filled');
        return false;
    }

    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['author']) AND isset($_POST['title']) AND isset($_POST['body'])){
        return true;
    }
    return false;
}

function fieldsArefilled($author, $title, $body){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($author) OR empty($body) OR empty($title)){
        return false;
    }
    return true;
}


function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}
